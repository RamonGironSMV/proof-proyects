﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Empleado : IEquatable<Empleado>, IComparable<Empleado>
    {
        //Atributos privados
        private int numero;
        private string nombre;
        private string sexo;
        private DateTime fechaNacimiento;
        private string departametno;
        //Propiedades publicas

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }

        }

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }

        }

        public string Departamento
        {
            get { return departametno; }
            set { departametno = value; }

        }

        //Metodo publico para mostara los datos
        public string MostrarDatos()
        {

            return ("\n\nNumero:" + Numero + "\nNombre" + Nombre + "\nSexo" + "\nFecha de nacimiento :" + FechaNacimiento.ToLongDateString() + "\nDepartamento: " + Departamento);
        }

        //Implementacion del metodo Equals () de la interfase IEquatable
        //Se utiliza este método para verificar que no existan empleados duplicados
        public bool Equals(Empleado otroEmpleado)
        {
            return (this.Numero == otroEmpleado.Numero);
        }

        //Implememtacion del metodo ComparetTo() de  la interfase IComparable
        //Se utilza  ete método para poder invocar el metodo Sort () de la clase generica List para ordenar alfabéticamete la lista de empleados
        public int CompareTo(Empleado otroEmpleado)
        {
            return (this.Nombre.CompareTo(otroEmpleado.Nombre));
        
        }
    
    }
}
